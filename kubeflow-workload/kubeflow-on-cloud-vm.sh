# /bin/bash

# Pre-req
curl -sSL https://gitlab.com/Utshab500/bash-scripts/-/raw/main/kubeflow-workload/pre-req.sh | bash

cd $HOME
git clone https://github.com/kubeflow/manifests.git
cd manifests

sed s/JWA_APP_SECURE_COOKIES=true/JWA_APP_SECURE_COOKIES=false/g ./apps/jupyter/jupyter-web-app/upstream/base/params.env | tee ./apps/jupyter/jupyter-web-app/upstream/base/params.env
sed s/TWA_APP_SECURE_COOKIES=true/TWA_APP_SECURE_COOKIES=false/g ./apps/tensorboard/tensorboards-web-app/upstream/base/params.env | tee ./apps/tensorboard/tensorboards-web-app/upstream/base/params.env
sed s/VWA_APP_SECURE_COOKIES=true/VWA_APP_SECURE_COOKIES=false/g ./apps/volumes-web-app/upstream/base/params.env | tee ./apps/volumes-web-app/upstream/base/params.env

while ! kustomize build example | kubectl apply -f -; do echo "Retrying to apply resources"; sleep 20; done

kubectl get po --all-namespaces