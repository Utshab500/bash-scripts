# /bin/bash

#############################
# Install Docker
#############################
curl -sSL https://gitlab.com/Utshab500/bash-scripts/-/raw/main/docker/docker-on-ubuntu.sh | bash

#############################
# Install kind
#############################
# For AMD64 / x86_64
[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.24.0/kind-linux-amd64
# For ARM64
[ $(uname -m) = aarch64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.24.0/kind-linux-arm64
chmod +x ./kind
mv ./kind /usr/local/bin/kind

kind --version
echo "Press any key to continue"
read x

#############################
# Install kubectl
#############################
apt-get install -y apt-transport-https ca-certificates curl gnupg

curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.31/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
chmod 644 /etc/apt/keyrings/kubernetes-apt-keyring.gpg

echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.31/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list
chmod 644 /etc/apt/sources.list.d/kubernetes.list 

apt-get update
apt-get install -y kubectl

kubectl version
echo "Press any key to continue"
read x

#############################
# Install kustomize
#############################
curl -sSL https://gitlab.com/Utshab500/bash-scripts/-/raw/main/kubeflow-workload/install-kustomize.sh | bash
cp kustomize /usr/bin/
kustomize --help
echo "Press any key to continue"
read x

#############################
# Deploy kubeflow workload
#############################
sysctl fs.inotify.max_user_instances=2280
sysctl fs.inotify.max_user_watches=1255360

# K8 cluster deployment
cat <<EOF | kind create cluster --name=kubeflow --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  image: kindest/node:v1.29.4
  kubeadmConfigPatches:
  - |
    kind: ClusterConfiguration
    apiServer:
      extraArgs:
        "service-account-issuer": "kubernetes.default.svc"
        "service-account-signing-key-file": "/etc/kubernetes/pki/sa.key"
EOF

kind get kubeconfig --name kubeflow > /tmp/kubeflow-config
export KUBECONFIG=/tmp/kubeflow-config