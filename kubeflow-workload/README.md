# Deploy instruction
Execute below commands
### Local kubeflow
```bash
curl -sSL https://gitlab.com/Utshab500/bash-scripts/-/raw/main/kubeflow-workload/kubeflow-on-local.sh | bash
```

### Kubeflow on cloud VM unsecure
```bash
curl -sS https://gitlab.com/Utshab500/bash-scripts/-/raw/main/kubeflow-workload/kubeflow-on-cloud-vm.sh | bash
```