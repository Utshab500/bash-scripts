#!/bin/bash

count=$(docker container ls | wc -l)
i=2
count=$(( count + 1 ))
while [[ "$i" -ne "$count" ]]
do
        container_id=$(docker container ls | sed -n `echo $i`p | awk '{ print $1 }')
        image_name=$(docker container ls | sed -n `echo $i`p | awk '{ print $2 }')
        container_name=$(docker container ls | sed -n `echo $i`p | awk '{ print $10 }')

        echo "Container ID = $container_id"
        echo "Image = $image_name"
        echo "Container Name = $container_name"

        docker container stop $container_id
        docker system prune -f
        docker pull $image_name
        docker run --name=$container_name -dit $image_name

        i=$(( i+1 ))
        echo $i
done
