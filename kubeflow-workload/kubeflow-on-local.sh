# /bin/bash

# Pre-req
curl -sSL https://gitlab.com/Utshab500/bash-scripts/-/raw/main/kubeflow-workload/pre-req.sh | bash

cd $HOME
git clone https://github.com/kubeflow/manifests.git
cd manifests
while ! kustomize build example | kubectl apply -f -; do echo "Retrying to apply resources"; sleep 20; done

kubectl get po --all-namespaces